import { ViewController, NavParams } from 'ionic-angular';

export class EditModalComponent {
  originalRecord: any;
  lookup: any;
  action: string;

  constructor(public viewController: ViewController, public navParams: NavParams) {
    this.originalRecord = this.navParams.get('record');
    this.action = this.navParams.get('action');
    this.lookup = this.navParams.get('lookup');
  }

  handleSaveEdit() {
    let data = { 'action': 'save', 'record': this.originalRecord};
    if (data.record.createdOn) {
      data.record.modifiedOn = new Date().toISOString();
    } else {
      data.record.createdOn = new Date().toISOString();
      data.record.modifiedOn = new Date().toISOString();
    }
    this.viewController.dismiss(data);
  }

  dismiss() {
    let data = { 'action': 'cancel'};
    this.viewController.dismiss(data);
  }
}
