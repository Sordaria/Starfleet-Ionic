import { OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

export class DetailPage implements OnInit {
  recordId: string;
  recordType: string;
  record: {} = {};
  lookup: {} = {};
  editModalComponent: any;
  action: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    this.recordId = this.navParams.get("recordId");
    this.action = this.navParams.get("action");
  }

  ngOnInit() {
    this.getInfo();
  }

  refresh() {
    this.getInfo();
  }

  getInfo() {
    let self : DetailPage = this;

    self.dataService.getRecord(self.recordType, self.recordId).then(function(result) {
      self.record = result;
      return self.lookupService.getLookups(self.recordType, self.record);
    }).then(function(result1) {
      self.lookup = result1;
    }).catch(function(error) {
      console.log(error);
    })
  }

  handleEdit() {
    let self: DetailPage = this;
    let editModal = self.modalCtrl.create(self.editModalComponent, {
      "record" : self.record,
      "lookup" : self.lookup,
      "action" : "edit"
    }, {
      enableBackdropDismiss: false
    });

    editModal.onDidDismiss(function(data) {
      if (data.action == "save") {
        self.dataService.updateRecord(self.recordType, data.record).then(function(result) {
          console.log("edit");
          self.refresh();
        }).catch(function(error) {
          console.log(error);
        });
      }
    });
    editModal.present();
  }

  ionViewWillLeave() {
    if (this.action == 'search') {
      this.navCtrl.popToRoot();
    }
  }

}
