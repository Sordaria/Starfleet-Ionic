import { OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { ListPage } from './list.page';

import jsonQuery from 'json-query';

export class SearchPage extends ListPage implements OnInit {
  searchFieldValue: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
  }

  ngOnInit() {

  }

  onInput(event: any) {
    if (event.key != "Enter" && event.target.value && event.target.value.length > 0) {
      this.filterValue = event.target.value;
      this.refresh();
    }
  }

  initList() {
    let self : SearchPage = this;
    let viewname : string = jsonQuery("recordTypes." + self.recordType + ".views." + self.viewName, {data: self.dataConfig}).value;

    self.dataService.getSearchList(self.recordType, viewname, self.filterValue).then(function(result1) {
      for (var i = 0; i < result1.rows.length; i++) {
        self.rows.push({recordId: "", recordType: "", record: result1.rows[i], lookup: null});
      }
    }).catch(function(error) {
      console.log(error);
    })
  }

  clickKey(key: any) {
    let self : SearchPage = this;
    let viewname : string = jsonQuery("recordTypes." + self.recordType + ".views." + self.viewName, {data: self.dataConfig}).value;

    self.dataService.getList(self.recordType, viewname, key).then(function(result) {
      self.navCtrl.push(self.detailPage, {
        recordId: result.rows[0].id,
        action: 'search'
      })
    }).catch(function(error) {
      console.log(error);
    })
  }
}
