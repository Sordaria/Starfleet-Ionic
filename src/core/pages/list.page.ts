import { OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import jsonQuery from 'json-query';
import async from 'async';


export class ListPage implements OnInit {
  dataConfig: any;
  count: number;
  rows: Array<{ recordId: string, recordType: string, record: any, lookup: any }>;
  recordType: string;
  editModalComponent: any;
  detailPage: any;
  searchPage: any;
  viewName: string;
  filterValue: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    this.dataConfig = this.configService.getDataConfig();
    this.rows = Array<{ recordId: string, recordType: string, record: any, lookup: any }>();
  }

  ngOnInit() {
    this.initList();
  }

  refresh() {
    this.rows = [];
    this.initList();
  }

  initList() {
    let self : ListPage = this;
    let viewname : string = jsonQuery("recordTypes." + self.recordType + ".views." + self.viewName, {data: self.dataConfig}).value;

    self.dataService.getListCount(self.recordType, viewname, self.filterValue).then(function(result) {
      self.count = result;
      return self.dataService.getList(self.recordType, viewname, self.filterValue);
    }).then(function(result1) {
      for (var i = 0; i < result1.rows.length; i++) {
        self.rows.push({recordId: result1.rows[i].id, recordType: self.recordType, record: result1.rows[i].value, lookup: {}});
      }
      self.buildLookups();
    }).catch(function(error) {
      console.log(error);
    })
  }

  handleNew() {
    let self: ListPage = this;
    let editModal = self.modalCtrl.create(self.editModalComponent, {
      "record" : {},
      "action" : "new",
      "lookup" : {}
    }, {
      enableBackdropDismiss: false
    });

    editModal.onDidDismiss(function(data) {
      if (data.action == "save") {
        self.dataService.createRecord(self.recordType, data.record).then(function(result) {
          console.log("save");
          self.refresh();
        }).catch(function(error) {
          console.log(error);
        });
      }
    });
    editModal.present();
  }

  buildLookups() {
    console.log('buildLookups');
    let self : ListPage = this;
    async.eachOfSeries(self.rows, function(row, key, callback) {
      console.log(row)
      self.lookupService.getLookups(row.recordType, row.record).then(function(result) {
        self.rows[key].lookup = result;
        callback();        
      }).catch(function(err) {
        console.error(err);
      })
    });
  }

  handleSearch() {
    this.navCtrl.push(this.searchPage);
  }

  clickOnRecord(row : any) {
    this.navCtrl.push(this.detailPage, {
      recordId: row.recordId
    });
  }

}
