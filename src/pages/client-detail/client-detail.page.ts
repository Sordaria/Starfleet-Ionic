import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { ClientEditModalComponent } from '../../components/client-edit-modal/client-edit-modal.component';
import { DetailPage } from '../../core/pages/detail.page';

@Component({
  selector: 'client-detail',
  templateUrl: 'client-detail.html'
})
export class ClientDetailPage extends DetailPage implements OnInit {
  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
    this.recordType = "client";
    this.editModalComponent = ClientEditModalComponent;
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
