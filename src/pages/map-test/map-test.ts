import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeolocationService } from '../../providers/geolocation.service';
import { MapService } from '../../providers/map.service';

/**
 * Generated class for the MapTestPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-map-test',
  templateUrl: 'map-test.html',
})
export class MapTestPage {

  centerPosition: any;
  marker = {
    firstname: null,
    lastname: null,
    lastConnection: null
  }
  userMarkers: Array<{position: any, record: any, recordId: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocationService: GeolocationService, public mapService: MapService, 
    public ngZone: NgZone) {
    this.centerPosition = "Paris, France";
    this.resetMarker();
  }

  ngOnInit() {
    window["googleMapPage"] = { component: this, zone: this.ngZone };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapTestPage');
  }

  resetMarker() {
    this.userMarkers = [];
  }

  myPositionClick() {
    let self: MapTestPage = this;
    console.log('MapTestPage - myPositionClick');
    self.resetMarker();
    self.geolocationService.getCurrentPosition().then(function(result) {
      console.log(result);
      self.centerPosition = result.latitude + ', ' + result.longitude;
      self.userMarkers.push({position: result.latitude + ', ' + result.longitude, record: null, recordId: null});
    }).catch(function(error) {
      console.error(error);
    });
  }

  userPosition() {
    let self: MapTestPage = this;
    console.log('MapTestPage userPosition');
    self.resetMarker();
    self.mapService.getUsersCoordinates().then(function(result) {
      for(var i =0; i < result.length; i++) {
        self.userMarkers.push({position: result[i].value.latitude + ', ' + result[i].value.longitude, record: result[i].value, recordId: result[i].id});
      }
    }).catch(function(error) {
      console.error(error);
    });
  }

  userMarkerClick(event, markerData) {
    let marker = event.target;
    console.log('userMarkerClick')
    console.log(markerData)
    this.marker.firstname = markerData.record.firstname;
    this.marker.lastname = markerData.record.lastname;
    this.marker.lastConnection = markerData.record.lastConnection;
    marker.nguiMapComponent.openInfoWindow('iw-person', marker);
  }

}
