import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { ListPage } from '../../core/pages/list.page';
import { PackageEditModalComponent } from '../../components/package-edit-modal/package-edit-modal.component';
import { PackageDetailPage } from '../package-detail/package-detail.page';
import { PackageSearchPage } from '../package-search/package-search.page';

@Component({
  selector: 'package-list',
  templateUrl: 'package-list.html'
})
export class PackageListPage extends ListPage implements OnInit {

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
    this.recordType = "package";
    this.viewName = "main";
    this.filterValue = "all";
    this.editModalComponent = PackageEditModalComponent;
    this.detailPage = PackageDetailPage;
    this.searchPage = PackageSearchPage;
  }

  ngOnInit() {
    super.initList();
  }

}
