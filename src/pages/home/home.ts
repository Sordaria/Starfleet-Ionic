import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  /* Graphique des ventes du mois */
 public barChartOptions: any = {
   scaleShowVerticalLines: false,
   responsive: true
 };
 public barChartLabels: string[] = ['November 2015', 'November 2016'];
 public barChartType: string = 'bar';
 public barChartLegend: boolean = true;

 public barChartData: any[] = [
   { data: [1120, 980], label: 'Cédric NOMENTSOA' },
   { data: [1560, 1620], label: 'David YOUBI' },
   { data: [1180, 1320], label: 'Ouss MILADY' }
 ];

 /* Graphique de l'historique des ventes */
 public lineChartData: Array<any> = [
   { data: [1230, 1156, 1325, 1456], label: 'Cédric NOMENTSOA' },
   { data: [1100, 1230, 1652, 1325], label: 'David YOUBI' },
   { data: [1230, 1450, 1320, 1230], label: 'Ouss MILADY' }
 ];
 public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April'];
 public lineChartOptions: any = {
   responsive: true
 };


 public lineChartLegend: boolean = true;
 public lineChartType: string = 'line';


 // events
 public chartClicked(e: any): void {
   console.log(e);
 }

 public chartHovered(e: any): void {
   console.log(e);
 }

}
