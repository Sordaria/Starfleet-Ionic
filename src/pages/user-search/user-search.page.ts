import { OnInit, Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { SearchPage } from '../../core/pages/search.page';
import { UserDetailPage } from '../user-detail/user-detail.page';

@Component({
  selector: 'user-search',
  templateUrl: 'user-search.html'
})
export class UserSearchPage extends SearchPage implements OnInit {
  searchFieldValue: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
    this.recordType = "user";
    this.viewName = "search";
    this.filterValue = "";
    this.detailPage = UserDetailPage;
  }
}
