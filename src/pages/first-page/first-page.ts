import { Component, OnInit } from '@angular/core';
import { ToastController, ModalController,  NavController, LoadingController } from 'ionic-angular';

import { LoginModalComponent } from '../../components/login-modal/login-modal.component';
import { AuthenticationService } from '../../providers/authentication.service';
import { HomePage } from '../home/home';
import { DriverHomePage } from '../driver-home/driver-home';
import { MapService } from '../../providers/map.service';

@Component({
  selector: 'first-page',
  templateUrl: 'first-page.html',
})

export class FirstPage implements OnInit {
  constructor(public toastController: ToastController, public modalController: ModalController, public navController: NavController,
  public authenticationService: AuthenticationService, public loading: LoadingController, private mapService: MapService) {

  }

  public ngOnInit() {
    let self: FirstPage = this;

    self.processAll();
  }

  processAll() {
    let self: FirstPage = this;

    let userData: any = {};
    Promise.resolve(userData).then(function(userData) {
      console.log("Check already auth");
      return self.promiseWaitAlreadyAuth(userData);
    }).then(function(userData) {
      console.log("Authentification");
      return self.promiseWaitLogin(userData);
    })/*.then(function(userData) {
      console.log("Verifications des données");
      return self.promiseWaitCheck(userData);
    })*/.then(function(userData) {
      console.log("Appel API");
      return self.promiseWaitCheckApi(userData);
    }).then(function(userData) {
      console.log("Ouverture de l'application");
      self.openHome();
    }).catch(function(error) {
      console.error(error);
      self.showError(error);
    });
  }

  promiseWaitAlreadyAuth(userData) {
    let self: FirstPage = this;

    return new Promise<any>(function(resolve, reject) {
      self.authenticationService.isAlreadyAuthentificated().then(function(resultCheck) {
        userData.skipLogin = resultCheck;
        resolve(userData);
      }).catch(function(error) {
        reject(error);
      });
    });
  }

  promiseWaitLogin(userData) {
    let self: FirstPage = this;

    return new Promise<any>(function(resolve, reject) {
      if (userData.skipLogin) {
        resolve(userData);
      } else {
        let modal = self.modalController.create(LoginModalComponent, {}, { enableBackdropDismiss: false});
        modal.onDidDismiss(function(data) {
          console.log("DEBUGGG")
          userData.loginResult = {
            username: data.username,
            password: data.password
          };
          resolve(userData);
        });
        modal.present();
      }
    });
  }

  // Fonctions à garder en cas d'authentification en local
  /*promiseWaitCheck(userData) {
    return new Promise<any>(function (resolve, reject) {
      if (userData.loginResult.username == "Cedric" && userData.loginResult.password == "Starfleet") {
        resolve(userData);
      }
      else {
        reject({ message: "Mauvais login ou mot de passe"});
      }
    });
  }*/

  promiseWaitCheckApi(userData) {
    let self: FirstPage = this;
    let loader = this.loading.create({
      spinner: 'hide',
      content: `
        <div class="loading-custom-spinner-container">
          <div class="loading-custom-spinner-box"></div>
        </div>
        <div>Authentification en cours...</div>`
    });

    return new Promise<any>(function (resolve, reject) {
      if (userData.skipLogin) {
        resolve(userData);
      } else {
        loader.present().then(() => {
          self.authenticationService.checkExternalLogin(userData.loginResult.username, userData.loginResult.password).then(function(data) {
            userData.username = userData.loginResult.username;
            loader.dismiss();
            resolve(userData);
          }).catch(function(error) {
            loader.dismiss();
            reject(error);
          });
        });
      }
    });
  }

  openHome() {
    let mode = this.authenticationService.getMode();
    let userId = this.authenticationService.getUserId();
    this.mapService.updateUser(userId);
    if (mode == "Administrateur") {
      this.navController.setRoot(HomePage);
    }
    else {
      this.navController.setRoot(DriverHomePage);
    }
  }

  showError(error) {
    let self: FirstPage = this;

    let toast = self.toastController.create({
      message: 'Erreur d\'authentification : ' + error.message,
      position: 'middle',
      showCloseButton: true,
      closeButtonText: "Ré-essayer"
    });

    toast.onDidDismiss(function() {
      self.processAll();
    });
    toast.present();
  }
}
