import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { ListPage } from '../../core/pages/list.page';
import { ClientEditModalComponent } from '../../components/client-edit-modal/client-edit-modal.component';
import { ClientDetailPage } from '../client-detail/client-detail.page';
import { ClientSearchPage } from '../client-search/client-search.page';

@Component({
  selector: 'client-list',
  templateUrl: 'client-list.html'
})
export class ClientListPage extends ListPage implements OnInit {

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
    this.recordType = "client";
    this.viewName = "main";
    this.filterValue = "all";
    this.editModalComponent = ClientEditModalComponent;
    this.detailPage = ClientDetailPage;
    this.searchPage = ClientSearchPage;
  }

  ngOnInit() {
    super.initList();
  }

}
