import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { ListPage } from '../../core/pages/list.page';
import { PackageDetailPage } from '../package-detail/package-detail.page';
import { AuthenticationService } from '../../providers/authentication.service';

@Component({
  selector: 'page-driver-home',
  templateUrl: 'driver-home.html'
})
export class DriverHomePage extends ListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
    public modalCtrl: ModalController, public authenticationService: AuthenticationService, public lookupService: LookupService) {
      super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
      this.recordType = "package";
      this.viewName = "driver";
      this.filterValue = this.authenticationService.getUserId();
      this.detailPage = PackageDetailPage;
  }

  ngOnInit() {
    super.initList();
  }

}
