import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { PackageEditModalComponent } from '../../components/package-edit-modal/package-edit-modal.component';
import { DetailPage } from '../../core/pages/detail.page';
import { UserDetailPage } from '../user-detail/user-detail.page';
import { ClientDetailPage } from '../client-detail/client-detail.page';

@Component({
  selector: 'package-detail',
  templateUrl: 'package-detail.html'
})
export class PackageDetailPage extends DetailPage implements OnInit {
  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
    this.recordType = "package";
    this.editModalComponent = PackageEditModalComponent;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  openUser(recordId : string) {
    this.navCtrl.push(UserDetailPage, {
      recordId: recordId
    });
  }

  openClient(recordId : string) {
    this.navCtrl.push(ClientDetailPage, {
      recordId: recordId
    });
  }
}
