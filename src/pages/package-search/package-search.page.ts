import { OnInit, Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { SearchPage } from '../../core/pages/search.page';
import { PackageDetailPage } from '../package-detail/package-detail.page';

@Component({
  selector: 'package-search',
  templateUrl: 'package-search.html'
})
export class PackageSearchPage extends SearchPage implements OnInit {
  searchFieldValue: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
    this.recordType = "package";
    this.viewName = "search";
    this.filterValue = "";
    this.detailPage = PackageDetailPage;
  }
}
