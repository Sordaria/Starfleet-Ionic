import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { ListPage } from '../../core/pages/list.page';
import { UserEditModalComponent } from '../../components/user-edit-modal/user-edit-modal.component';
import { UserDetailPage } from '../user-detail/user-detail.page';
import { UserSearchPage } from '../user-search/user-search.page';

@Component({
  selector: 'user-list',
  templateUrl: 'user-list.html'
})
export class UserListPage extends ListPage implements OnInit {

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
    this.recordType = "user";
    this.viewName = "main";
    this.filterValue = "all";
    this.editModalComponent = UserEditModalComponent;
    this.detailPage = UserDetailPage;
    this.searchPage = UserSearchPage;
  }

  ngOnInit() {
    super.initList();
  }

}
