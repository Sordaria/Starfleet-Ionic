import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { UserEditModalComponent } from '../../components/user-edit-modal/user-edit-modal.component';
import { DetailPage } from '../../core/pages/detail.page';

@Component({
  selector: 'user-detail',
  templateUrl: 'user-detail.html'
})
export class UserDetailPage extends DetailPage implements OnInit {
  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
    this.recordType = "user";
    this.editModalComponent = UserEditModalComponent;
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
