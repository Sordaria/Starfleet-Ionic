import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { DriverHomePage } from '../pages/driver-home/driver-home';
import { UserListPage } from '../pages/user-list/user-list.page';
import { ClientListPage } from '../pages/client-list/client-list.page';
import { PackageListPage } from '../pages/package-list/package-list.page';
import { FirstPage } from '../pages/first-page/first-page';
import { MapTestPage } from '../pages/map-test/map-test';

import { AuthenticationService } from '../providers/authentication.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  subscriptionEndAuth: any = null;

  pages: Array<{title: string, component: any, icon: string, isSelected: boolean}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public authenticationService: AuthenticationService) {
    let self : MyApp = this;

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', component: HomePage, icon: 'home', isSelected: true }
    ];
    this.rootPage = FirstPage;

    // Refresh page on refresh
    this.subscriptionEndAuth  = this.authenticationService.endAuth.subscribe(function(online) {
      self.reloadMenu();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  reloadMenu() {
    console.log("RELOAD LA PAGE");
    let mode = this.authenticationService.getMode();
    this.pages = [];
    console.log("Utilisateur en mode " + mode);
    if (mode == "Administrateur") {
      this.pages.push({title: 'Dashboard', component: HomePage, icon: 'home', isSelected: true});
      this.pages.push({title: 'Chauffeurs', component: UserListPage, icon: 'person', isSelected: false});
      this.pages.push({title: 'Clients', component: ClientListPage, icon: 'ios-person', isSelected: false});
      this.pages.push({title: 'Colis', component: PackageListPage, icon: 'cube', isSelected: false});
      this.pages.push({title: 'Map', component: MapTestPage, icon: 'map', isSelected: false});
    } else {
      this.pages.push({title: 'Dashboard', component: DriverHomePage, icon: 'home', isSelected: true});
    }
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);

    for(let p of this.pages) {
      p.isSelected = false;
    }
    page.isSelected = true;
  }

  handleLogOut() {
    this.authenticationService.logOut();
  }
}
