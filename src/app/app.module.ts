import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

/* Pages */
import { HomePage } from '../pages/home/home';
import { DriverHomePage } from '../pages/driver-home/driver-home';
import { UserListPage } from '../pages/user-list/user-list.page';
import { UserDetailPage } from '../pages/user-detail/user-detail.page';
import { UserSearchPage } from '../pages/user-search/user-search.page';
import { ClientListPage } from '../pages/client-list/client-list.page';
import { ClientDetailPage } from '../pages/client-detail/client-detail.page';
import { ClientSearchPage } from '../pages/client-search/client-search.page';
import { PackageListPage } from '../pages/package-list/package-list.page';
import { PackageDetailPage } from '../pages/package-detail/package-detail.page';
import { PackageSearchPage } from '../pages/package-search/package-search.page';
import { FirstPage } from '../pages/first-page/first-page';
import { MapTestPage } from '../pages/map-test/map-test';
import { NavigationPage } from '../pages/navigation/navigation';

/* Components */
import { LoginModalComponent } from '../components/login-modal/login-modal.component';
import { UserEditModalComponent } from '../components/user-edit-modal/user-edit-modal.component';
import { UserSelectorModal } from '../components/user-selector-modal/user-selector-modal.component';
import { UserTableComponent } from '../components/user-table-component/user-table-component';
import { ClientEditModalComponent } from '../components/client-edit-modal/client-edit-modal.component';
import { ClientSelectorModal } from '../components/client-selector-modal/client-selector-modal.component';
import { PackageEditModalComponent } from '../components/package-edit-modal/package-edit-modal.component';
import { PackageSelectorModal } from '../components/package-selector-modal/package-selector-modal.component';
import { PackageTableComponent } from '../components/package-table-component/package-table-component';

/* Providers */
import { AuthenticationService } from '../providers/authentication.service';
import { ConfigService } from '../providers/config.service';
import { DataService } from '../providers/data.service';
import { LookupService } from '../providers/lookup.service';
import { GeolocationService } from '../providers/geolocation.service';
import { MapService } from '../providers/map.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ChartsModule } from 'ng2-charts';
import { NguiMapModule} from '@ngui/map';

@NgModule({
  declarations: [
    MyApp,
    DriverHomePage,
    HomePage,
    UserListPage,
    UserDetailPage,
    UserSearchPage,
    UserEditModalComponent,
    UserSelectorModal,
    UserTableComponent,
    ClientListPage,
    ClientDetailPage,
    ClientSearchPage,
    ClientEditModalComponent,
    ClientSelectorModal,
    PackageListPage,
    PackageDetailPage,
    PackageSearchPage,
    PackageEditModalComponent,
    PackageSelectorModal,
    PackageTableComponent,
    FirstPage,
    NavigationPage,
    LoginModalComponent,
    MapTestPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ChartsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyAn8yxvOqW0PtyUh7tkLRzqYTXMCAaY97M'})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DriverHomePage,
    UserListPage,
    UserDetailPage,
    UserSearchPage,
    UserEditModalComponent,
    UserSelectorModal,
    ClientListPage,
    ClientDetailPage,
    ClientSearchPage,
    ClientEditModalComponent,
    ClientSelectorModal,
    PackageListPage,
    PackageDetailPage,
    PackageSearchPage,
    PackageEditModalComponent,
    PackageSelectorModal,
    FirstPage,
    MapTestPage,
    NavigationPage,
    LoginModalComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthenticationService,
    DataService,
    ConfigService,
    LookupService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GeolocationService,
    Geolocation,
    MapService
  ]
})
export class AppModule {}
