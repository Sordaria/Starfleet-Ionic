import { Component, OnInit, Input } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { ListPage } from '../../core/pages/list.page';
import { UserDetailPage } from '../../pages/user-detail/user-detail.page';


@Component({
  selector: 'user-table-component',
  templateUrl: 'user-table-component.html'
})
export class UserTableComponent extends ListPage implements OnInit {

    @Input()
    recordType: string;

    constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
    public modalCtrl: ModalController, public lookupService: LookupService) {
        super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
        console.log(this.recordType)
        this.viewName = "main";
        this.filterValue = "all";
        this.detailPage = UserDetailPage;
    }

    ngOnInit() {
        super.initList();
    }

}
