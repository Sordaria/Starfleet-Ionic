import { Component } from '@angular/core';
import { ViewController, NavParams, ModalController } from 'ionic-angular';

import { EditModalComponent } from '../../core/components/modal/edit.modal.component';
import { UserSelectorModal } from '../user-selector-modal/user-selector-modal.component';
import { ClientSelectorModal } from '../client-selector-modal/client-selector-modal.component'

@Component({
  selector: 'package-edit-modal',
  templateUrl: 'package-edit-modal.html'
})

export class PackageEditModalComponent extends EditModalComponent {
  constructor(public viewController: ViewController, public navParams: NavParams, public modalCtrl: ModalController) {
    super(viewController, navParams);
  }

  clearClick() {
    console.log("clear");
    this.lookup.driver_name = null;
  }


  searchClick() {
    let self : PackageEditModalComponent = this;
    let modal = self.modalCtrl.create(UserSelectorModal, {}, {
      enableBackdropDismiss: false
    });

    modal.onDidDismiss(function(data) {
      self.lookup.driver_name = data.label;
      self.originalRecord.driver_id = data.id;
    });
    modal.present();
  }

  clearClickClient() {
    console.log("clear");
    this.lookup.receiver_name = null;
  }


  searchClickClient() {
    let self : PackageEditModalComponent = this;
    let modal = self.modalCtrl.create(ClientSelectorModal, {}, {
      enableBackdropDismiss: false
    });

    modal.onDidDismiss(function(data) {
      self.lookup.receiver_name = data.label;
      self.originalRecord.receiver_id = data.id;
    });
    modal.present();
  }
}
