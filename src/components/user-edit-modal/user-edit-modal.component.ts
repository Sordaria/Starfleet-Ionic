import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

import { EditModalComponent } from '../../core/components/modal/edit.modal.component';

import * as crypt from 'crypto-js/sha256'

@Component({
  selector: 'user-edit-modal',
  templateUrl: 'user-edit-modal.html'
})

export class UserEditModalComponent extends EditModalComponent {

  constructor(public viewController: ViewController, public navParams: NavParams) {
    super(viewController, navParams);
  }

  handleSaveEdit() {
    let data = { 'action': 'save', 'record': this.originalRecord};
    if (this.action == 'new') {
      data.record.password = crypt(data.record.password).toString();
    }
    this.viewController.dismiss(data);
  }
}
