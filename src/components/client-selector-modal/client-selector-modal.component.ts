import { OnInit, Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';

import { DataService } from '../../providers/data.service';
import { ConfigService } from '../../providers/config.service';
import { LookupService } from '../../providers/lookup.service';

import { SearchPage } from '../../core/pages/search.page';

import jsonQuery from 'json-query';

@Component({
  selector: 'client-selector-modal',
  templateUrl: 'client-selector-modal.html'
})
export class ClientSelectorModal extends SearchPage implements OnInit {
  searchFieldValue: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public configService: ConfigService, public dataService: DataService,
  public modalCtrl: ModalController, public viewCtrl: ViewController, public lookupService: LookupService) {
    super(navCtrl, navParams, configService, dataService, modalCtrl, lookupService);
    this.recordType = "client";
    this.viewName = "search";
    this.filterValue = "";
  }

  clickKey(key: any) {
    let self : ClientSelectorModal = this;
    let viewname : string = jsonQuery("recordTypes." + self.recordType + ".views." + self.viewName, {data: self.dataConfig}).value;

    self.dataService.getList(self.recordType, viewname, key).then(function(result) {
      let data = {
        'action': 'select',
        'id': result.rows[0].id,
        'label': result.rows[0].value.lastname
      }
      self.viewCtrl.dismiss(data);
    }).catch(function(error) {
      console.log(error);
    })
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
