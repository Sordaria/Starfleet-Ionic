import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
  selector: 'login-modal',
  templateUrl: 'login-modal.html'
})

export class LoginModalComponent {
  username: string;
  password: string;

  constructor(public viewController: ViewController) {

  }

  handleLogin() {
    let data = {action: 'login', 'username': this.username, 'password': this.password};
    this.viewController.dismiss(data);
  }
}
