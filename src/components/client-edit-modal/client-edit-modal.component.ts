import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

import { EditModalComponent } from '../../core/components/modal/edit.modal.component';

@Component({
  selector: 'client-edit-modal',
  templateUrl: 'client-edit-modal.html'
})

export class ClientEditModalComponent extends EditModalComponent {
  constructor(public viewController: ViewController, public navParams: NavParams) {
    super(viewController, navParams);
  }
}
