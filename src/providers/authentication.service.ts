import { Injectable, EventEmitter } from '@angular/core';
import { Storage } from '@ionic/storage';

import { DataService } from './data.service';
import { ConfigService } from './config.service';

import * as crypt from 'crypto-js/sha256'

@Injectable()
export class AuthenticationService {

  dataConfig: any;
  mode: string;
  username: string;
  userId: string;
  endAuth: EventEmitter<any> = new EventEmitter();

  constructor(private dataService: DataService, private storage: Storage, private configService: ConfigService) {
    this.dataConfig = this.configService.getDataConfig();
  }

  public isAlreadyAuthentificated() {
    let self: AuthenticationService = this;
    let user: string = null;
    let mode: string = null;

    return new Promise<any>(function(resolve, reject) {
      self.storage.get('username').then(function(username) {
        console.log("username " + username);
        user = username;
        return self.storage.get('mode');
      }).then(function(getmode) {
        console.log("mode " + getmode);
        mode = getmode;
        return self.storage.get('userId');
      }).then(function(userId) {
        console.log("userId " + userId);
        if (mode == null || user == null || userId == null) {
          resolve(false);
        } else {
          self.setOptions(user, mode, userId);
          resolve(true);
        }
      }).catch(function() {
        resolve(false);
      });
    });
  }

  public checkExternalLogin(username, password) {
    let self: AuthenticationService = this;
    console.log("AuthenticationService ici");

    return new Promise<any>(function (resolve, reject) {
      self.getAuthenfication(username, password).then(function(response) {
        self.setStorageAuth(response.user, response.mode, response._id);
        self.setOptions(response.user, response.mode, response._id);
        resolve(response);
      }).catch(function(error){
        reject({message: error});
      });
    });
  }
  /** Version CouchDB 1.* */
  /* private getAuthenfication(username: string, password: string) {
    let self : AuthenticationService = this;
    let viewname : string = jsonQuery("recordTypes.user.views.main", {data: self.dataConfig}).value;

    return new Promise<any>(function(resolve, reject) {
      self.dataService.getListCount("user", viewname, username).then(function(result) {
        if (result == 0) {
          console.log("Pas d'utilisateur trouvé");
          reject("Pas d'utilisateur trouvé");
        } else {
          console.log("Utilisateur trouvé");
          return self.dataService.getList("user", viewname, username);
        }
      }).then(function(result1) {
        console.log("Liste récupéré");
        var userId = result1.rows[0].id;
        return self.dataService.getRecord("user", userId);
      }).then(function(result2) {
        console.log("Utilisateur récupéré");
        if (result2.password == crypt(password).toString()) {
          resolve(result2)
        } else {
          reject("Mot de passe erroné");
        }
      }).catch(function(error) {
        reject(error);
      })
    });
  } */

  /** Version CouchDB 2.* */
  private getAuthenfication(username: string, password: string) {
    let self : AuthenticationService = this;

    return new Promise<any>(function(resolve, reject) {
      self.dataService.findRecord("user", {
        selector: {"user": username}
      }).then(function(result) {
        console.log('getAuthenfication');
        console.log(result);
        if (result.docs.length == 0) {
          console.log("Pas d'utilisateur trouvé");
          reject("Pas d'utilisateur trouvé");
        } else if (result.docs[0].password == crypt(password).toString()) { //A ENLEVER QUAND YAURA LE BACK !!!
          console.log("Bon mot de passe");
          resolve(result.docs[0]);
        } else {
          reject("Mot de passe erroné");
        }
      }).catch(function(error) {
        reject(error);
      })
    });
  }

  private setStorageAuth(username : string, mode : string, userId : string) {
    this.storage.set('username', username);
    this.storage.set('mode', mode);
    this.storage.set('userId', userId);
  }

  private setOptions(username : string, mode : string, userId : string) {
    this.mode = mode;
    this.username = username;
    this.userId = userId;
    this.endAuth.emit();
  }

  public getMode() {
    return this.mode;
  }

  public getUserId() {
    return this.userId;
  }

  public getUsername() {
    return this.username;
  }

  public logOut() {
    let self: AuthenticationService = this;

    self.storage.remove('username').then(function() {
      return self.storage.remove('mode');
    }).then(function() {
      return self.storage.remove('userId');
    }).then(function() {
      window.location.reload();
    }).catch(function(error) {
      console.log(error)
    });
  }
}
