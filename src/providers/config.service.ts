import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {

  private authenticationConfig : {};
  private serverConfig: {};
  private dataConfig: {};

  constructor() {
    this.initAuthenticationConfig();
    this.initServerConfig();
    this.initDataConfig();
  }

  private initServerConfig() {
    this.serverConfig = {
      "url": "http://localhost:5984/"
    }
  }

  private initAuthenticationConfig() {
    this.authenticationConfig = {
      "api": "http://api.omiladi.tk/login"
    }
  }

  private initDataConfig() {
    this.dataConfig = {
      "recordTypes": {
        "user": {
          "dbname": "starfleet_user",
          "views" : {
            "main": "records/list",
            "search": "records/search",
            "map": "records/bycoords"
          }
        },
        "client": {
          "dbname": "starfleet_client",
          "views" : {
            "main": "records/list",
            "search": "records/search"
          }
        },
        "package": {
          "dbname": "starfleet_package",
          "views" : {
            "main": "records/list",
            "search": "records/search",
            "driver": "records/bydriver"
          },
          "lookups": {
            "user": {
              "lookupRecordType": "user",
              "lookupIdentify": "driver_id",
              "lookupName": "driver_name",
              "lookupToIdentify": "lastname"
            },
            "client": {
              "lookupRecordType": "client",
              "lookupIdentify": "receiver_id",
              "lookupName": "receiver_name",
              "lookupToIdentify": "lastname"
            }
          }
        }
      }
    }
  }

  public getAuthenficationConfig() {
    return this.authenticationConfig;
  }

  public getServerConfig() {
    return this.serverConfig;
  }

  public getDataConfig() {
    return this.dataConfig;
  }

}
