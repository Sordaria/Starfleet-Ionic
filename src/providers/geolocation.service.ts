import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';

import request from 'request';

@Injectable()
export class GeolocationService {
    constructor(protected geolocation: Geolocation) {

    }

    public getCurrentPosition() {
        console.log('GeolocationService - getCurrentPosition');
        let self : GeolocationService = this;
        return new Promise<any>(function(resolve, reject) {
            self.geolocation.getCurrentPosition().then(function(result) {
                var coord = {};
                coord['latitude'] = result.coords.latitude;
                coord['longitude'] = result.coords.longitude;
                console.log(coord);
                resolve(coord);
            }).catch(function(error) {
                console.error(error);
                reject(error);
            });
        });
        /*return new Promise<any>(function(resolve, reject) {
            let url : string =  'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyAn8yxvOqW0PtyUh7tkLRzqYTXMCAaY97M';
            request.post({url: url, timeout: 5000}, function (error, response, body) {

                if (!error && body) {
                    console.log("GeolocationService.getCurrentPositionFromGoogle body:" +body);

                    let jsonBody = JSON.parse(body);
                    if (jsonBody.location) {
                        var coord = {};
                        coord['latitude'] = jsonBody.location.lat;
                        coord['longitude'] = jsonBody.location.lng;
                        console.log(coord);
                        resolve(coord);
                    }
                }
                reject(error);
            });
        });*/
    }

}