import { Injectable } from '@angular/core';

import { ConfigService } from './config.service';

import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);
import jsonQuery from 'json-query';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class DataService {

  serverConfig: any;
  dataConfig: any;

  constructor(private configService: ConfigService) {
    this.serverConfig = this.configService.getServerConfig();
    this.dataConfig = this.configService.getDataConfig();
  }

  public getRecord(recordType: string, recordId: string) {
    let self : DataService = this;
    let db = self.getPouchdbObject(recordType);

    return new Promise<any>(function(resolve, reject) {
      db.get(recordId).then(function(res) {
        console.log(res);
        resolve(res);
      }).catch(function(error) {
        reject(error);
      });
    });
  }

  public findRecord(recordType: string, data: object) {
    let self : DataService = this;
    let db = self.getPouchdbObject(recordType);

    return new Promise<any>(function(resolve, reject) {
      db.find(data).then(function(res) {
        console.log(res);
        resolve(res);
      }).catch(function(error) {
        reject(error);
      });
    });
  }

  public getList(recordType: string, viewname: string, filterValue: string) {
    let self : DataService = this;
    let db = self.getPouchdbObject(recordType);

    return new Promise<any>(function(resolve, reject) {
      db.query(viewname, {
        'startkey': [filterValue],
        'endkey': [filterValue, {}],
        'reduce': false
      }).then(function(res) {
        resolve(res);
      }).catch(function(error) {
        reject(error);
      });
    });
  }

  public getSearchList(recordType: string, viewname: string, filterValue: string) {
    let self : DataService = this;
    let db = self.getPouchdbObject(recordType);

    return new Promise<any>(function(resolve, reject) {
      db.query(viewname, {
        'group_level': 1,
        'startkey': [filterValue],
        'endkey': [filterValue + '\ufff0'],
      }).then(function(res) {
        resolve(res);
      }).catch(function(error) {
        reject(error);
      });
    });
  }

  public getListCount(recordType: string, viewname: string, filterValue: string) {
    let self : DataService = this;
    let db = self.getPouchdbObject(recordType);

    return new Promise<any>(function(resolve, reject) {
      db.query(viewname, {
        'startkey': [filterValue],
        'endkey': [filterValue, {}]
      }).then(function(res) {
        if (res.rows[0]) {
          resolve(res.rows[0].value);
        } else {
          resolve(0);
        }
      }).catch(function(error) {
        reject(error);
      });;
    });
  }

  public createRecord(recordType: string, record: any) {
    let self : DataService = this;
    let db = self.getPouchdbObject(recordType);

    record["recordType"] = recordType;
    return db.post(record);
  }

  public updateRecord(recordType: string, record: any) {
    let self : DataService = this;
    let db = self.getPouchdbObject(recordType);

    record["recordType"] = recordType;
    return db.put(record);
  }

  private getPouchdbObject(recordType: string) {
    let self : DataService = this;
    let urlServer : string = jsonQuery("url", {data: self.serverConfig}).value;
    let dbname : string = jsonQuery("recordTypes." + recordType + ".dbname", {data: self.dataConfig}).value;
    let db = new PouchDB(urlServer + dbname);

    console.log(urlServer + dbname);

    return db;
  }

}
