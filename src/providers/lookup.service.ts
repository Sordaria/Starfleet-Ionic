import { Injectable } from '@angular/core';

import { ConfigService } from './config.service';
import { DataService } from './data.service';

import jsonQuery from 'json-query';

@Injectable()
export class LookupService {

  dataConfig: any;

  constructor(private configService: ConfigService, private dataService: DataService) {
    this.dataConfig = this.configService.getDataConfig();
  }

  getLookups(recordType: string, record: any) {
    let self: LookupService = this;
    let listLookups = jsonQuery("recordTypes." + recordType + ".lookups", {data: self.dataConfig}).value;
    let resultLookup = {};
    let promises = [];

    console.log("get lookups");
    console.log(listLookups);

    for (let key in listLookups) {
      for (let keylookup in record) {
        if (keylookup == listLookups[key]["lookupIdentify"]) {
          promises.push(self.dataService.getRecord(listLookups[key]["lookupRecordType"], record[listLookups[key]["lookupIdentify"]]));
        }
      }
    }

    return new Promise<any>(function(resolve, reject) {
      let allPromises = Promise.all(promises);
      allPromises.then(function(datas) {
        console.log("get lookup record");
        for (let data of datas) {
          resultLookup[listLookups[data.recordType]["lookupName"]] = data[listLookups[data.recordType]["lookupToIdentify"]];
        }
        console.log(resultLookup);
        resolve(resultLookup);
      }).catch(function(error) {
        console.log(error);
      });
    });
  }
}
