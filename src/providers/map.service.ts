import { Injectable } from '@angular/core';

import { DataService } from './data.service';
import { GeolocationService } from './geolocation.service';
import { ConfigService } from './config.service';

import jsonQuery from 'json-query';

@Injectable()
export class MapService {
    dataConfig: any;

    constructor(private dataService: DataService, private geolocationService: GeolocationService, private configService: ConfigService) {
        this.dataConfig = this.configService.getDataConfig();
    }

    public updateUser(userId: string) {
        console.log('MapService updateUser');
        console.log('userID ' + userId);
        let self : MapService = this;
        var record: any;
        self.dataService.getRecord('user', userId).then(function(result) {
            console.log(result);
            record = result;
            return self.geolocationService.getCurrentPosition();
        }).then(function(coord) {
            record['latitude'] = coord.latitude;
            record['longitude'] = coord.longitude;
            record['lastConnection'] = new Date().toISOString();
            return self.dataService.updateRecord('user', record);
        }).then(function(res) {
            console.log('UPDATE USER COORD');
            console.log(res);
        }).catch(function(error) {
            console.error(error);

        })
        
    }

    public getUsersCoordinates() {
        console.log('MapService getUserCoordinates');
        let self: MapService = this;
        let recordType : string = 'user';
        let view : string = 'map';
        let viewname : string = jsonQuery("recordTypes." + recordType + ".views." + view, {data: self.dataConfig}).value;
        return new Promise<any>(function(resolve, reject) {
            self.dataService.getList(recordType, viewname, 'all').then(function(result) {
                console.log(result);
                resolve(result.rows)
            }).catch(function(error) {
                console.error(error);
                reject(error);
            });
        });
    }

}