This is a starter template for [Ionic](http://ionicframework.com/docs/) projects.

## How to use this template

Pour installer l'application, il faut :

* Installer [NodeJS](https://nodejs.org/en/)

* Installer Ionic et Cordova (à faire éventuellement en mode root sur Linux)
```bash
$ npm install -g ionic cordova
```

* Cloner ce dépot sur votre ordinateur

* Aller dans le dossier cloné et installer les dépendances
```bash
$ npm install
```

* Lancer le mode développement
```bash
$ ionic serve
```
