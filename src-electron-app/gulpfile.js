
var gulp = require('gulp');
var del = require('del');
var copy  = require('gulp-copy');


var gulp = require('gulp');
var del = require('del');

gulp.task('clean:electron', function () {
  return del([
    'app/www/**/*'
  ]);
});

gulp.task('copy:electron', ['clean:electron'], function () {
  return gulp.src('../www/**').pipe(gulp.dest('./app/www'));
});

gulp.task('prepare:electron', ['clean:electron', 'copy:electron']);


gulp.task('copy:electron:dist', ['clean:electron'], function () {
  return gulp.src('../platforms/browser/www/**').pipe(gulp.dest('./app/www'));
});

gulp.task('prepare:electron:dist', ['clean:electron', 'copy:electron:dist']);
